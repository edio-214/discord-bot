# TODO count mentions

# Telegram bot management
import time
import telepot as tp
from telepot.loop import MessageLoop

# .env management
import os
from dotenv import load_dotenv

# Discord bot management
import discord
from discord.ext import commands

# Obtain a unique universal id for registration
import uuid

# MongoDB
from pymongo import MongoClient, errors

# use a try-except indentation to catch MongoClient() errors
try:
    # mongoClient = MongoClient('mongodb://mongodb:27017/')
    mongoClient = MongoClient('mongodb://localhost:27017/')

    # print the version of MongoDB server if connection successful
    print ("MongoDB server connection successful - server version:", mongoClient.server_info()["version"])

    # get the database_names from the MongoClient()
    database_names = mongoClient.list_database_names()

except errors.ServerSelectionTimeoutError as err:
    # set the client and DB name list to 'None' and `[]` if exception
    mongoClient = None
    database_names = []

    # catch pymongo.errors.ServerSelectionTimeoutError
    print ("pymongo ERROR:", err)

print ("\nMongoDB available databases:", database_names)

# mongoClient = MongoClient('mongodb://localhost:27017/')
uclBotDB = mongoClient['uclBotDatabase']
users = uclBotDB['users']

# .env extraction
load_dotenv()
TELEGRAM_TOKEN = os.getenv('TELEGRAM_TOKEN')
DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')

# Print all registers in the database
# import pprint
# for user in users.find():
#     pprint.pprint(user)

discordBot = commands.Bot(command_prefix='!')
discordBot.remove_command('help')

import asyncio
_loop = asyncio.get_event_loop()

async def sendMessageDiscord(discordID, message):
    user = await discordBot.fetch_user(discordID)
    await user.send(message)

def on_TMessage(discordID, message):
    asyncio.run_coroutine_threadsafe(
        sendMessageDiscord(discordID, message), _loop)

# Telegram bot commands
def parse_command(command_string):
    text_split = command_string.split()
    return text_split[0], text_split[1:]

def command_start(chat_id, params):
    response = "Olá Codelabber! Para saber o que consigo fazer use o comando /help"
    telegramBot.sendMessage(chat_id, response)

def command_help(chat_id, params):
    response = """Olá, Codelabber!

Integro as mentions do Discord com o Telegram
Registrando, ficará sabendo sempre que foi mencionado sem depender das notificações inconsistentes do Discord, especialmente no mobile

Comandos disponíveis:
    `/iniciarRegistro` - devolve um hash para ser usado em conjunto com `/finalizarRegistro` no Telegram, integrando as plataformas
    `/finalizarRegistro <hash>` - usado com o hash do comando `/iniciarRegistro` do Telegram para concluir a integração das plataformas
    `/checarRegistro` - devolve seu estado atual em relação à integração de plataformas
    `/deletarRegistro` - remove a sua integração entre plataformas, cessando as notificações no Telegram
"""
    telegramBot.sendMessage(chat_id, response, parse_mode='Markdown')

def command_begin_registration(chat_id, params):
    telegramUserRegistered = False

    query = { 'telegramID': chat_id }
    if users.count_documents(query) > 0:
        telegramUserRegistered = True

    if not telegramUserRegistered:
        registrationHash = uuid.uuid4().hex
        username = telegramBot.getChat(chat_id)['username']
        response = (
            f'Olá {username}, execute o seguinte comando no UCLbot do Discord:\n'
            f'`!finalizarRegistro {registrationHash}`\n'
        )
        user = { 'hash': registrationHash, 'telegramID': chat_id, 'discordID': None }
        returnID = users.insert_one(user).inserted_id
    else:
        response = (
            'Você já está registrado ou em processo de registro\n'
            'Para remover seu registro e parar de receber mensagens no Telegram:\n'
            'Use o comando /deletarRegistro'
        )
    telegramBot.sendMessage(chat_id, response, parse_mode='Markdown')

def command_finish_registration(chat_id, params):
    params_hash = params[0]
    userFound = False

    query = {'hash': params_hash }
    if users.count_documents(query) > 0:
        userFound = True
        alteration = { '$set': { 'telegramID': chat_id} }
        users.update_one(query, alteration)
        currentUserDiscordID = users.find_one(query)['discordID']

    if userFound:
        # Remove possivel duplicidade caso o comando begin tenha sido executado em ambas plataformas
        query = { 'telegramID': chat_id, 'discordID': None }
        users.delete_one(query)

        response = ('O registro com o Discord foi concluído\n'
            'Você recebeu uma mensagem de confirmação no Discord também')
        on_TMessage(currentUserDiscordID, 'Registro concluído')
    else:
        response = 'Não há pedido de registro para esse hash'
    telegramBot.sendMessage(chat_id, response)

def command_check_registration(chat_id, params):
    userRegistering = False
    userRegistered = False
    
    query = { 'telegramID': chat_id }
    if users.count_documents(query) > 0:
        user = users.find_one(query)
        if user['discordID'] == None:
            userRegistering = True
        else:
            userRegistered = True

    if userRegistered:
        response = 'Você já está registrado com o Discord'
    elif userRegistering:
        response = 'Você está em processo de registro, termine no Discord'
    else:
        response = 'Você não iniciou o processo de registro ainda'
    telegramBot.sendMessage(chat_id, response)

def command_delete_registration(chat_id, params):
    userRemoved = False

    query = { 'telegramID': chat_id }
    if users.count_documents(query) > 0:
        users.delete_one(query)
        userRemoved = True

    if userRemoved:
        response = 'Você foi removido da integração com o Discord'
    else:
        response = 'Você não foi removido por não estar registrado ainda'
    telegramBot.sendMessage(chat_id, response)

telegram_commands = {}
telegram_commands['/start'] = command_start
telegram_commands['/help'] = command_help
telegram_commands['/iniciarRegistro'] = command_begin_registration
telegram_commands['/finalizarRegistro'] = command_finish_registration
telegram_commands['/checarRegistro'] = command_check_registration
telegram_commands['/deletarRegistro'] = command_delete_registration

# Telegram bot
def handle(msg):
    # pylint: disable=unbalanced-tuple-unpacking
    content_type, chat_type, chat_id = tp.glance(msg)
    print(content_type, chat_type, chat_id)

    if content_type == 'text':
        msg_text = msg['text']

        if msg_text[0] == '/':
            command, params = parse_command(msg_text)
            try:
                telegram_commands[command](chat_id, params)
            except KeyError:
                telegramBot.sendMessage(chat_id, f'Comando inválido: {command}')
        else:
            telegramBot.sendMessage(chat_id, 'A mensagem não começa com um comando')

telegramBot = tp.Bot(TELEGRAM_TOKEN)

MessageLoop(telegramBot, handle).run_as_thread()

@discordBot.event
async def on_ready():
    print('\nDiscord Servers available:')
    for guild in discordBot.guilds:
        print(guild)
    await discordBot.change_presence(activity=discord.Game(name='Codelab lalalala \U0001F3B6'))
    print(f'\n{discordBot.user} has connected')

@discordBot.command(name='iniciarRegistro')
async def discordBeginRegistration(ctx):
    discordUserRegistered = False

    query = { 'discordID': ctx.author.id }
    if users.count_documents(query) > 0:
        discordUserRegistered = True

    if not discordUserRegistered:
        registrationHash = uuid.uuid4().hex
        response = (
            f'Olá <@!{ctx.author.id}>, execute o seguinte comando no UCLbot do Telegram:'
            f'```/finalizarRegistro {registrationHash}```'
        )
        user = { 'hash': registrationHash, 'telegramID': None, 'discordID': ctx.author.id }
        returnID = users.insert_one(user).inserted_id
    else:
        response = (
            'Você já está registrado ou em processo de registro\n'
            'Para remover seu registro e parar de receber mensagens no Telegram:\n'
            'Use o comando ```!deletarRegistro```'
        )
    await ctx.send(response)

@discordBot.command(name='finalizarRegistro')
async def discordFinishRegistration(ctx, userHash):
    userFound = False
    query = { 'hash': userHash }
    if users.count_documents(query) > 0:
        userFound = True
        alteration = { '$set': { 'discordID': ctx.author.id} }
        users.update_one(query, alteration)
        currentUserTelegramID = users.find_one(query)['telegramID']

    if userFound:
        # Remove possivel duplicidade caso o comando begin tenha sido executado em ambas plataformas
        query = { 'discordID': ctx.author.id, 'telegramID': None }
        users.delete_one(query)

        response = ('O registro com o Telegram foi concluído\n'
            'Você recebeu uma mensagem de confirmação no Telegram também')
        telegramBot.sendMessage(currentUserTelegramID, 'Registro concluído')
    else:
        response = 'Não há pedido de registro para esse hash'
    await ctx.send(response)

@discordBot.command(name='checarRegistro')
async def discordCheckRegistration(ctx):
    userRegistering = False
    userRegistered = False

    query = { 'discordID': ctx.author.id }
    if users.count_documents(query) > 0:
        user = users.find_one(query)
        if user['telegramID'] == None:
            userRegistering = True
        else:
            userRegistered = True

    if userRegistered:
        response = 'Você já está registrado com o Telegram'
    elif userRegistering:
        response = 'Você está em processo de registro, termine no Telegram'
    else:
        response = 'Você não iniciou o processo de registro ainda'

    await ctx.send(response)

@discordBot.command(name='deletarRegistro')
async def discordDeleteRegistration(ctx):
    userRemoved = False

    query = { 'discordID': ctx.author.id }
    deletedRegisters = users.delete_one(query).deleted_count

    if deletedRegisters:
        userRemoved = True

    if userRemoved:
        response = 'Você foi removido da integração com o Telegram'
    else:
        response = 'Você não foi removido por não estar registrado ainda'

    await ctx.send(response)

@discordBot.command(name='help')
async def discordHelp(ctx):
    response = """Olá, Codelabber!

Integro as mentions do Discord com o Telegram
Registrando, ficará sabendo sempre que foi mencionado sem depender das notificações inconsistentes do Discord, especialmente no mobile

Comandos disponíveis:
    `!iniciarRegistro` - devolve um hash para ser usado em conjunto com `/finalizarRegistro` no Telegram, integrando as plataformas
    `!finalizarRegistro <hash>` - usado com o hash do comando `/iniciarRegistro` do Telegram para concluir a integração das plataformas
    `!checarRegistro` - devolve seu estado atual em relação à integração de plataformas
    `!deletarRegistro` - remove a sua integração entre plataformas, cessando as notificações no Telegram
"""
    await ctx.send(response)

def queryMessageUser(member, message):
    query = { 'discordID': member.id }
    user = users.find_one(query)
    currentUserTelegramID = None
    if user != None:
        currentUserTelegramID = user['telegramID']

    if currentUserTelegramID != None:
        response = (
            f'Codelabber, você foi mencionado por `{message.author}`'
            f' no canal `{message.channel}` do servidor `{message.guild}`'
        )
        telegramBot.sendMessage(currentUserTelegramID, response, parse_mode='Markdown')

@discordBot.event
async def on_message(message):
    if message.mention_everyone:
        for member in message.guild.members:
            queryMessageUser(member, message)
    for role in message.role_mentions:
        for member in role.members:
            queryMessageUser(member, message)
    for member in message.mentions:
        queryMessageUser(member, message)
    await discordBot.process_commands(message)

discordBot.run(DISCORD_TOKEN)

# Keep server running
while True:
    time.sleep(3)